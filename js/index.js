var counterContainer = document.querySelector(".counter"); 
var visitCount = localStorage.getItem("page_view");

// Check for current page views
if (visitCount){
    visitCount = Number(visitCount) +1; 
    localStorage.setItem("page_view", visitCount);
} else {
    visitCount = 1; 
    localStorage.setItem("page_view", 1);
}
counterContainer.innerHTML = visitCount; 


